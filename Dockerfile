FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server:latest

## software needed

## When using official way given by https://rustup.rs, hard links are not used for
## executable files in cargo/bin, instead, files are duplicated

#RUN \
#     curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs \
#     | sh -s -- --default-toolchain stable -y

## Borrowed from https://hub.docker.com/r/rustlang/rust

ENV RUSTUP_HOME=/opt/rust/rustup \
    CARGO_HOME=/opt/rust/cargo \
    PATH=/opt/rust/cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

WORKDIR /opt

RUN \
    /bin/sh -c set -eux \
 && dpkgArch="$(dpkg --print-architecture)" \
 && case "${dpkgArch##*-}" in \
      amd64) rustArch='x86_64-unknown-linux-gnu' ;; \
      arm64) rustArch='aarch64-unknown-linux-gnu' ;; \
          *) echo >&2 "unsupported architecture: ${dpkgArch}"; exit 1 ;; \
    esac \
 && url="https://static.rust-lang.org/rustup/dist/${rustArch}/rustup-init" \
 && curl -o rustup-init "$url" \
 && chmod +x rustup-init \
 && ./rustup-init -y --no-modify-path --default-toolchain nightly \
 && rm rustup-init \
 && chmod -R a+w $RUSTUP_HOME $CARGO_HOME
 
# RUST extension for VS Code

RUN \
  /app/openvscode-server/bin/openvscode-server \
      --extensions-dir /init-config/.openvscode-server/extensions \
      --install-extension rust-lang.rust-analyzer

