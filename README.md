# docker-openvscode-server-rust

## What

This image is based on [docker-openvscode-server](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-openvscode-server),
it adds the needed tools for Rust development.

Included tools are:
- Rustup installation
- Rust VS Code extension


## Details

- The exposed port is 3000
- The user folder is `/config`
- the user and sudo password is `abc`
- if docker is installed on your computer, you can run (amd64 or arm64 architecture) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-rust`

